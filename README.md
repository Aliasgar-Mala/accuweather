# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This is an exciting app that gets today's weather info and 3 day forecast along with news highlight

### Architecture and Design? ###

I have used MVVM architecture keeping modularization and scaling in mind and following the SOLID principle. VM binding with its view is done using generic Observable called Box

I have also used a factory pattern (ViewControllerFactory) to have easy access to all viewcontroller at one place. so that viewcontrollers can be used/reused without worrying about underlying details

I have divided the HomeViewController into different child view controllers,so that each piece can operate independently (have its own lifecycle)and also be reused(modularization).

I have done all my views in code, so no xib or storyboard. Images are loaded asynchronously on a separate thread for lazy loading and smooth scrolling with caching capabilities.

I have a generic network layer, and the API for web calls are defined in Protocol for easier mocking.

### Who do I talk to? ###

for any questions please contact me at aliasgar.mala@gmail.com

### Points to note ###
Maximum and Minimum is calculated by iterration the temperatures of the day and finding respective max and min
Similarly icon for 3 day forecast is calculated based on the maximum time that weather condition appears

### Things that can be improved ###

Loading of cities can be done on background thread and would make sense if we had multi page application.

Search can be improve massively, currently it is loading everything and when search changes it can does the search all over again (hence slow), we can cache the partial search and make it faster

we can achieve pagination for news

can make the entire page scrollable and not just the news