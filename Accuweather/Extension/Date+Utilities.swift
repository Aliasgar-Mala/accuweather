//
//  Date+Utilities.swift
//  Accuweather
//
//  Created by Aliasgar Mala on 2020-06-22.
//  Copyright © 2020 Aliasgar Mala. All rights reserved.
//

import Foundation

extension Date {
    func getDatePlus(numberOfDays: Int) -> String {
        var result = ""
        
        let theCalendar = Calendar.current
        var dayComponent    = DateComponents()
        dayComponent.day = numberOfDays
        let nextDate = theCalendar.date(byAdding: dayComponent, to: self)
        if let nextDate = nextDate {
            result = DateFormatter.standardDateFormatter.string(from: nextDate)
        }
        return result
    }
}
