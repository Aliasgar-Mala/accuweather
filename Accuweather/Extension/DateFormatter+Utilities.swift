//
//  DateFormatter+Utilities.swift
//  Accuweather
//
//  Created by Aliasgar Mala on 2020-06-22.
//  Copyright © 2020 Aliasgar Mala. All rights reserved.
//

import Foundation

extension DateFormatter {
    
    static let standardDateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "YYYY-MM-dd"
        return formatter
    }()
    
}
