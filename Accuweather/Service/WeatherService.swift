//
//  WeatherService.swift
//  Accuweather
//
//  Created by Aliasgar Mala on 2020-06-17.
//  Copyright © 2020 Aliasgar Mala. All rights reserved.
//

import Foundation

protocol WeatherService {
    func fetchWeatherInfo(cityId: Int, completion: @escaping (Result<WeatherInfo, ServiceLayerClient.ServiceError>) -> Void)
    
    func get3dayForecast(cityId: Int, completion: @escaping (Result<ForecastWeather, ServiceLayerClient.ServiceError>) -> Void)
}

extension ServiceLayerClient: WeatherService {
    func get3dayForecast(cityId: Int, completion: @escaping (Result<ForecastWeather, ServiceError>) -> Void) {
        
        let cityForecast = CityForecast(id: String(cityId))
        createDataTask(baseURL: Endpoint.baseURLWeather.path, path: Endpoint.weatherForecast.path, parameter: cityForecast) {
            completion($0)
        }

    }
    
    func fetchWeatherInfo(cityId: Int, completion: @escaping (Result<WeatherInfo, ServiceError>) -> Void) {
        let cityForecast = CityForecast(id: String(cityId))
        createDataTask(baseURL: Endpoint.baseURLWeather.path, path: Endpoint.currentWeather.path, parameter: cityForecast) {
            completion($0)
        }
    }
}


