//
//  Endpoint.swift
//  Accuweather
//
//  Created by Aliasgar Mala on 2020-06-17.
//  Copyright © 2020 Aliasgar Mala. All rights reserved.
//

import Foundation

enum Endpoint {
    case baseURLWeather
    case baseURLNews
    case currentWeather
    case weatherForecast
    case weatherImage(iconId: String)
    case topHeadlines
    
    var path: String {
        switch self {
        case .baseURLWeather:
            return "https://api.openweathermap.org/data/"
        case .baseURLNews:
            return "https://newsapi.org/v2/"
        case .currentWeather:
            return "2.5/weather"
        case .weatherForecast:
            return "2.5/forecast"
        case .weatherImage(let iconId):
            return "https://openweathermap.org/img/w/\(iconId).png"
        case .topHeadlines:
            return "top-headlines"
        }
    }
}
