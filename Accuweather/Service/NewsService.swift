//
//  NewsService.swift
//  Accuweather
//
//  Created by Aliasgar Mala on 2020-06-21.
//  Copyright © 2020 Aliasgar Mala. All rights reserved.
//

import Foundation

protocol NewsService {
    func getNews(completion: @escaping (Result<News, ServiceLayerClient.ServiceError>) -> Void)
}

extension ServiceLayerClient: NewsService {
    
    func getNews(completion: @escaping (Result<News, ServiceError>) -> Void) {
        let inputModel = NewsModel()
        createDataTask(baseURL: Endpoint.baseURLNews.path, path: Endpoint.topHeadlines.path, parameter: inputModel) {
            completion($0)
        }
    }
}
