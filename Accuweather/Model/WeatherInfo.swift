//
//  CurrentWeather.swift
//  Accuweather
//
//  Created by Aliasgar Mala on 2020-06-17.
//  Copyright © 2020 Aliasgar Mala. All rights reserved.
//

import Foundation

// MARK: - WeatherInfo
struct WeatherInfo: Decodable {
    let weather: [Weather]
    let main: Main
    let name: String
}

struct ForeCastInfo: Decodable {
    let weather: [Weather]
    let main: Main
    let dateText: String
    
    enum CodingKeys: String, CodingKey {
        case weather, main
        case dateText = "dtTxt"
    }
}

// MARK: - Main
struct Main: Decodable {
    let temp: Double
    let tempMin, tempMax: Double
}

// MARK: - Weather
struct Weather: Decodable {
    let id: Int
    let main, icon: String

    enum CodingKeys: String, CodingKey {
        case id, main
        case icon
    }
}
