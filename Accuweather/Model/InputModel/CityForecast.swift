//
//  CityForecast.swift
//  Accuweather
//
//  Created by Aliasgar Mala on 2020-06-17.
//  Copyright © 2020 Aliasgar Mala. All rights reserved.
//

import Foundation

struct CityForecast: Encodable {
    let id: String
    //this should ideally be stored in keychain
    let appId = "6ad6726f4d3d745e204889063cf30fbf"
    let units = "metric"
}
