//
//  City.swift
//  Accuweather
//
//  Created by Aliasgar Mala on 2020-06-17.
//  Copyright © 2020 Aliasgar Mala. All rights reserved.
//

import Foundation

struct City: Decodable, Hashable {
    let id: Int
    let name, state, country: String
}
