//
//  News.swift
//  Accuweather
//
//  Created by Aliasgar Mala on 2020-06-21.
//  Copyright © 2020 Aliasgar Mala. All rights reserved.
//

import Foundation

struct News: Decodable {
    let articles: [Article]
}
// MARK: - Article
struct Article: Decodable {
    let source: Source
    let title: String
    let urlToImage: String?
}

// MARK: - Source
struct Source: Decodable {
    let id: String?
    let name: String
}
