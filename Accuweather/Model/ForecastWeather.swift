//
//  ForecastWeather.swift
//  Accuweather
//
//  Created by Aliasgar Mala on 2020-06-20.
//  Copyright © 2020 Aliasgar Mala. All rights reserved.
//

import Foundation

struct ForecastWeather: Decodable {
    let threeHourlyWeatherInfo: [ForeCastInfo]
    
    enum CodingKeys: String, CodingKey {
        case list
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        threeHourlyWeatherInfo = try container.decode([ForeCastInfo].self, forKey: .list)
    }
}
