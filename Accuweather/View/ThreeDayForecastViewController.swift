//
//  CurrentWeaatherViewController.swift
//  Accuweather
//
//  Created by Aliasgar Mala on 2020-06-20.
//  Copyright © 2020 Aliasgar Mala. All rights reserved.
//

import UIKit

class ThreeDayForecastViewController: UIViewController {
    
    let threeDayForecastViewModel: ThreeDayForecastViewModel

    private var mainView: ThreeDayForeCastView {
        return self.view as! ThreeDayForeCastView
    }

    override public func loadView() {
        self.view = ThreeDayForeCastView()
    }
    
    init(threeDayForecastViewModel: ThreeDayForecastViewModel) {
        self.threeDayForecastViewModel = threeDayForecastViewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureBindings()
    }
    
    private func configureBindings() {
        
        threeDayForecastViewModel.isLoading.bind { [weak self] isLoading in
            isLoading ? self?.view.showActivityIndicator() : self?.view.hideActivityIndicator()
        }
        
        threeDayForecastViewModel.isResponseLoaded.bind { [weak self] (isResponseLoaded) in
            guard let self = self, isResponseLoaded else {return}
            self.mainView.bind(viewModel: self.threeDayForecastViewModel)
        }
    }

}
