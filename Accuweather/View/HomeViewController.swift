//
//  ViewController.swift
//  Accuweather
//
//  Created by Aliasgar Mala on 2020-06-17.
//  Copyright © 2020 Aliasgar Mala. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    
    private var searchController: UISearchController!
    
    private let viewControllerFactory: ViewControllerFactory

    private let serviceLayerClient = ServiceLayerClient()
    
    private let selectCityView: SelectCityView
   
    private let newsViewController: UIViewController
    
    private let weatherViewController: WeatherViewController
    
    private let threeDayForecastViewController: ThreeDayForecastViewController
    
    init(viewControllerFactory: ViewControllerFactory) {
        self.viewControllerFactory = viewControllerFactory
        newsViewController = viewControllerFactory.newsViewController()
        weatherViewController = viewControllerFactory.weatherViewController()
        threeDayForecastViewController = viewControllerFactory.threeDayForecastViewController()
        selectCityView = SelectCityView()
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initializeSearchViewController() {
        let resultViewController = SearchResultViewController()
        resultViewController.delegate = self
        searchController = UISearchController(searchResultsController: resultViewController)
        searchController.searchResultsUpdater = resultViewController
        searchController.searchBar.placeholder = "Search in your city"
        navigationItem.searchController = searchController
    }
    
    //can be extracted as a separate view file
    private func setupEmptyView() {
        let guide = view.safeAreaLayoutGuide
        
        selectCityView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(selectCityView)
        
        NSLayoutConstraint.activate([
            selectCityView.leadingAnchor.constraint(equalTo: guide.leadingAnchor, constant: 15),
            selectCityView.topAnchor.constraint(equalTo: guide.topAnchor, constant: 15),
            selectCityView.trailingAnchor.constraint(equalTo: guide.trailingAnchor, constant: -15),
            selectCityView.heightAnchor.constraint(equalToConstant: 280)
        ])
    }
    
    private func initializeNewsViewController() {
        addChild(newsViewController)
        newsViewController.view.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(newsViewController.view)
        newsViewController.didMove(toParent: self)
        
        let guide = view.safeAreaLayoutGuide
        NSLayoutConstraint.activate([
            newsViewController.view.leadingAnchor.constraint(equalTo: guide.leadingAnchor, constant: 15),
            newsViewController.view.topAnchor.constraint(equalTo: selectCityView.bottomAnchor, constant: 15),
            newsViewController.view.trailingAnchor.constraint(equalTo: guide.trailingAnchor, constant: -15),
            newsViewController.view.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemGray5
        title = "Accuweather"
        initializeSearchViewController()
        setupEmptyView()
        initializeNewsViewController()
    }
}

extension HomeViewController: SearchResultNavigationDelegate {
    
    private func addWeatherViewController() {
        addChild(weatherViewController)
        weatherViewController.view.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(weatherViewController.view)
        weatherViewController.didMove(toParent: self)
        
        let guide = view.safeAreaLayoutGuide

        NSLayoutConstraint.activate([
            weatherViewController.view.leadingAnchor.constraint(equalTo: guide.leadingAnchor, constant: 15),
            weatherViewController.view.topAnchor.constraint(equalTo: guide.topAnchor, constant: 15),
            weatherViewController.view.trailingAnchor.constraint(equalTo: guide.trailingAnchor, constant: -15),
        ])
    }
    
    func addForecastViewController() {
        addChild(threeDayForecastViewController)
        threeDayForecastViewController.view.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(threeDayForecastViewController.view)
        threeDayForecastViewController.didMove(toParent: self)
        
        let guide = view.safeAreaLayoutGuide

        NSLayoutConstraint.activate([
            threeDayForecastViewController.view.leadingAnchor.constraint(equalTo: guide.leadingAnchor, constant: 15),
            threeDayForecastViewController.view.topAnchor.constraint(equalTo: weatherViewController.view.bottomAnchor, constant: 15),
            threeDayForecastViewController.view.trailingAnchor.constraint(equalTo: guide.trailingAnchor, constant: -15),
        ])
    }
    
    private func adjustNewsViewControllerConstraint() {
        
        let guide = view.safeAreaLayoutGuide
        NSLayoutConstraint.activate([
            newsViewController.view.leadingAnchor.constraint(equalTo: guide.leadingAnchor, constant: 15),
            newsViewController.view.topAnchor.constraint(equalTo: threeDayForecastViewController.view.bottomAnchor, constant: 15),
            newsViewController.view.trailingAnchor.constraint(equalTo: guide.trailingAnchor, constant: -15),
            newsViewController.view.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
    }
    func navigateToHomeWith(selectedCityId: Int) {
        
        selectCityView.removeFromSuperview()
        searchController.searchBar.text = ""
        
        weatherViewController.weatherViewModel.cityId.value = selectedCityId
        threeDayForecastViewController.threeDayForecastViewModel.cityId.value = selectedCityId
        
        addWeatherViewController()
        addForecastViewController()
        adjustNewsViewControllerConstraint()
    }
}

