//
//  SelectCityView.swift
//  Accuweather
//
//  Created by Aliasgar Mala on 2020-06-22.
//  Copyright © 2020 Aliasgar Mala. All rights reserved.
//

import UIKit

class SelectCityView: UIView {

    private let titleLabel = UILabel()
    
    init() {
        super.init(frame: CGRect.zero)
        postInit()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func postInit() {
        
        tintColor = .black
        backgroundColor = .white
        layoutMargins = UIEdgeInsets.zero
        accessibilityViewIsModal = true
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.text = "Please pick a city to view weather details"
        addSubview(titleLabel)
        
        
        titleLabel.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        titleLabel.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        titleLabel.textAlignment = .center
    }
}
