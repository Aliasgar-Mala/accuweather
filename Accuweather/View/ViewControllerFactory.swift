//
//  ViewControllerFactory.swift
//  Accuweather
//
//  Created by Aliasgar Mala on 2020-06-22.
//  Copyright © 2020 Aliasgar Mala. All rights reserved.
//

import UIKit
//Factory pattern which has handy for using / re-using the view controller though out the app
class ViewControllerFactory {

    private let service: ServiceLayerClient

    init(service: ServiceLayerClient) {
        self.service = service
    }
    
    func weatherViewController() -> WeatherViewController {
        let weatherViewModel = WeatherViewModel(weatherService: service, imageService: service)
        let weatherViewController = WeatherViewController(weatherViewModel: weatherViewModel)
        return weatherViewController
    }
    
    func threeDayForecastViewController() -> ThreeDayForecastViewController {
        
        let threeDayForecastViewModel = ThreeDayForecastViewModel(weatherService: service, imageService: service)
        let threeDayForecastViewController = ThreeDayForecastViewController(threeDayForecastViewModel: threeDayForecastViewModel)
        return threeDayForecastViewController
    }
    
    func newsViewController() -> UIViewController {
        let newsViewModel = NewsViewModel(newsService: service, imageService: service)
        let newsViewController = NewsTableViewController(viewModel: newsViewModel)
        return newsViewController
    }
}
