//
//  SearchResultViewController.swift
//  Accuweather
//
//  Created by Aliasgar Mala on 2020-06-17.
//  Copyright © 2020 Aliasgar Mala. All rights reserved.
//

import UIKit

protocol SearchResultNavigationDelegate: class {
    func navigateToHomeWith(selectedCityId: Int)
}

class SearchResultViewController: UITableViewController, UISearchResultsUpdating {
    
    weak var delegate: SearchResultNavigationDelegate?
    
    var cities: [City] = AccuweatherSetup.shared.cities
    var filteredCities: [City] = [] {
        didSet {
            tableView.reloadData()
        }
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        guard let text = searchController.searchBar.text else { return }
        view.showActivityIndicator()
        DispatchQueue.global(qos: .background).async {
            let localCities = self.cities.filter { $0.name.lowercased().contains(text.lowercased()) }
            DispatchQueue.main.async {
                self.view.hideActivityIndicator()
                self.filteredCities = localCities
            }
        }
    }
    
    init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredCities.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellIdentifier") ?? UITableViewCell(style: .default, reuseIdentifier: "cellIdentifier")
        
        cell.textLabel?.text = filteredCities[indexPath.row].name
        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedCity = filteredCities[indexPath.row]
        delegate?.navigateToHomeWith(selectedCityId: selectedCity.id)
        dismiss(animated: true)
    }
}
