//
//  ThreeDayForeCastView.swift
//  Accuweather
//
//  Created by Aliasgar Mala on 2020-06-20.
//  Copyright © 2020 Aliasgar Mala. All rights reserved.
//

import UIKit

class ThreeDayForeCastView: UIView {
    
    private let cardView: UIView = UIView()
    
    private let textLabel: UILabel = UILabel()
    
    private let horizontalTemperatureStackView: UIStackView = {
        let horizontalStackView = UIStackView()
        horizontalStackView.axis = .horizontal
        horizontalStackView.alignment = .fill
        horizontalStackView.distribution = .equalSpacing
        horizontalStackView.translatesAutoresizingMaskIntoConstraints = false
        return horizontalStackView
    }()
    
    private let verticalStackView1: UIStackView = {
        let verticalStackView = UIStackView()
        verticalStackView.axis = .vertical
        verticalStackView.alignment = .fill
        verticalStackView.spacing = 5
        verticalStackView.translatesAutoresizingMaskIntoConstraints = false
        return verticalStackView
    }()
    
    private let thumbnailView1: UIImageView = UIImageView()
    private let temperatureLabel1: UILabel = UILabel()
    
    private let verticalStackView2: UIStackView = {
        let verticalStackView = UIStackView()
        verticalStackView.axis = .vertical
        verticalStackView.alignment = .fill
        verticalStackView.spacing = 5
        verticalStackView.translatesAutoresizingMaskIntoConstraints = false
        return verticalStackView
    }()
    
    private let thumbnailView2: UIImageView = UIImageView()
    private let temperatureLabel2: UILabel = UILabel()
    private let verticalStackView3: UIStackView = {
        let verticalStackView = UIStackView()
        verticalStackView.axis = .vertical
        verticalStackView.alignment = .fill
        verticalStackView.spacing = 5
        verticalStackView.translatesAutoresizingMaskIntoConstraints = false
        return verticalStackView
    }()
    private let thumbnailView3: UIImageView = UIImageView()
    private let temperatureLabel3: UILabel = UILabel()
    
    
    init() {
        super.init(frame: CGRect.zero)
        postInit()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func postInit() {
        configureView()
        setupConstraints()
    }
    
    private func configureView() {
        
        cardView.backgroundColor = .white
        cardView.layer.cornerRadius = 2.0
        cardView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        cardView.translatesAutoresizingMaskIntoConstraints = false
        cardView.layer.shadowOpacity = 0.1
        addSubview(cardView)
        
        textLabel.font = UIFont.systemFont(ofSize: 18)
        textLabel.translatesAutoresizingMaskIntoConstraints = false
        cardView.addSubview(textLabel)
        
        cardView.addSubview(horizontalTemperatureStackView)
        
        thumbnailView1.contentMode = .scaleAspectFit
        thumbnailView1.translatesAutoresizingMaskIntoConstraints = false
        verticalStackView1.addArrangedSubview(thumbnailView1)
        
        temperatureLabel1.translatesAutoresizingMaskIntoConstraints = false
        verticalStackView1.addArrangedSubview(temperatureLabel1)
        
        thumbnailView2.contentMode = .scaleAspectFit
        thumbnailView2.translatesAutoresizingMaskIntoConstraints = false
        verticalStackView2.addArrangedSubview(thumbnailView2)
        
        temperatureLabel2.translatesAutoresizingMaskIntoConstraints = false
        verticalStackView2.addArrangedSubview(temperatureLabel2)
        
        thumbnailView3.contentMode = .scaleAspectFit
        thumbnailView3.translatesAutoresizingMaskIntoConstraints = false
        verticalStackView3.addArrangedSubview(thumbnailView3)
        
        temperatureLabel3.translatesAutoresizingMaskIntoConstraints = false
        verticalStackView3.addArrangedSubview(temperatureLabel3)
        
        horizontalTemperatureStackView.addArrangedSubview(verticalStackView1)
        horizontalTemperatureStackView.addArrangedSubview(verticalStackView2)
        horizontalTemperatureStackView.addArrangedSubview(verticalStackView3)
        
    }
    
    private func setupConstraints() {
        cardView.pinToEdges(of: self, padding: UIEdgeInsets(top: 5, left: 10, bottom: 10, right: 5))
        
        NSLayoutConstraint.activate([
            textLabel.topAnchor.constraint(equalTo: cardView.topAnchor, constant: 30),
            textLabel.leadingAnchor.constraint(equalTo: cardView.leadingAnchor, constant: 30)
        ])
        
        NSLayoutConstraint.activate([
            horizontalTemperatureStackView.topAnchor.constraint(equalTo: textLabel.bottomAnchor, constant: 20),
            horizontalTemperatureStackView.leadingAnchor.constraint(equalTo: cardView.leadingAnchor, constant: 30),
            horizontalTemperatureStackView.bottomAnchor.constraint(equalTo: cardView.bottomAnchor, constant: -20),
            horizontalTemperatureStackView.trailingAnchor.constraint(equalTo: cardView.trailingAnchor, constant: -20)
        ])
    }
    
    
    func bind(viewModel: ThreeDayForecastViewModel) {
        
        textLabel.text = "Next 3 days"
        
        temperatureLabel1.text = viewModel.minMax1
        
        viewModel.image1.bind {  [weak self] in
            self?.thumbnailView1.image = $0
        }
        
        temperatureLabel2.text = viewModel.minMax2
        
        viewModel.image2.bind {  [weak self] in
            self?.thumbnailView2.image = $0
        }
        
        temperatureLabel3.text = viewModel.minMax3
        
        viewModel.image3.bind {  [weak self] in
            self?.thumbnailView3.image = $0
        }
    }
    
}
