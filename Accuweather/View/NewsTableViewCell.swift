//
//  NewsViewCellTableViewCell.swift
//  Accuweather
//
//  Created by Aliasgar Mala on 2020-06-21.
//  Copyright © 2020 Aliasgar Mala. All rights reserved.
//

import UIKit

class NewsTableViewCell: UITableViewCell {
    
    private let verticalStackView = UIStackView()
    private let thumbnailView = UIImageView()
    private let title = UILabel()
    private let publisher = UILabel()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        postInit()
    }
     
     required init?(coder: NSCoder) {
         fatalError("init(coder:) has not been implemented")
     }
     
     private func postInit() {
        configureView()
        setupConstraints()
     }
     
     private func configureView() {
         
        thumbnailView.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(thumbnailView)

        verticalStackView.axis = .vertical
        verticalStackView.alignment = .fill
        verticalStackView.spacing = 3
        verticalStackView.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(verticalStackView)

        title.translatesAutoresizingMaskIntoConstraints = false
        title.numberOfLines = 0
        title.lineBreakMode = .byWordWrapping
        title.setContentHuggingPriority(.required, for: .vertical)
        title.setContentCompressionResistancePriority(.defaultLow, for: .vertical)
        title.font = UIFont.boldSystemFont(ofSize: 20)
        verticalStackView.addArrangedSubview(title)


        publisher.translatesAutoresizingMaskIntoConstraints = false
        publisher.font = UIFont.systemFont(ofSize: 18)
        verticalStackView.addArrangedSubview(publisher)
     }
     
     private func setupConstraints() {
         
         //maintaining the aspect ratio of portrait image
         NSLayoutConstraint.activate([
             thumbnailView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 12),
             thumbnailView.heightAnchor.constraint(equalToConstant: 80),
             thumbnailView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
             thumbnailView.widthAnchor.constraint(equalTo: thumbnailView.heightAnchor, multiplier: 3/2),
         ])
         
         NSLayoutConstraint.activate([
             verticalStackView.leadingAnchor.constraint(equalTo: thumbnailView.trailingAnchor, constant: 15),
             verticalStackView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10),
             verticalStackView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -10),
             verticalStackView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor)
         ])
     }
     
     func bind(viewModel: NewsRowViewModel) {
         
        // showing default image when the image is loading
        thumbnailView.image = nil
        viewModel.thumbnail.bind { [weak self] in
            self?.thumbnailView.image = $0
        }

        title.text = viewModel.title
        publisher.text = viewModel.publisher
     }

}
