//
//  WeatherViewController.swift
//  Accuweather
//
//  Created by Aliasgar Mala on 2020-06-20.
//  Copyright © 2020 Aliasgar Mala. All rights reserved.
//

import UIKit

class WeatherViewController: UIViewController {
    let weatherViewModel: WeatherViewModel

    private var mainView: WeatherView {
        return self.view as! WeatherView
    }

    override public func loadView() {
        self.view = WeatherView()
    }
    
    init(weatherViewModel: WeatherViewModel) {
        self.weatherViewModel = weatherViewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureBindings()
    }
    
    private func configureBindings() {
        
        weatherViewModel.isLoading.bind { [weak self] isLoading in
            isLoading ? self?.view.showActivityIndicator() : self?.view.hideActivityIndicator()
        }
        
        weatherViewModel.isResponseLoaded.bind { [weak self] (isResponseLoaded) in
            guard let self = self, isResponseLoaded else { return }
            self.mainView.bind(viewModel: self.weatherViewModel)
        }
    }
}
