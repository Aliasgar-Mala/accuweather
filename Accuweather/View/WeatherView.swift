//
//  WeatherView.swift
//  Accuweather
//
//  Created by Aliasgar Mala on 2020-06-17.
//  Copyright © 2020 Aliasgar Mala. All rights reserved.
//

import UIKit

class WeatherView: UIView {
    
    private let verticalStackView = UIStackView()
    private let todayInLabel = UILabel()
    private let titleLabel = UILabel()
    
    private let horizontalTemperatureStackView = UIStackView()
    private let maxAndMinLabel = UILabel()
    private let currentLabel = UILabel()
    private let imageView = UIImageView()
    
    init() {
        super.init(frame: CGRect.zero)
        postInit()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func postInit() {
        configureView()
        setupConstraints()
    }
    
    private func configureView() {
    
        tintColor = .black
        layoutMargins = UIEdgeInsets.zero
        accessibilityViewIsModal = true
        
        verticalStackView.axis = .vertical
        verticalStackView.alignment = .leading
        verticalStackView.spacing = 5
        verticalStackView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(verticalStackView)
        
        todayInLabel.translatesAutoresizingMaskIntoConstraints = false
        verticalStackView.addArrangedSubview(todayInLabel)
        
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.lineBreakMode = .byWordWrapping
        titleLabel.font = UIFont.boldSystemFont(ofSize: 30)
        titleLabel.setContentHuggingPriority(.required, for: .vertical)
        titleLabel.setContentCompressionResistancePriority(.required, for: .vertical)
        titleLabel.numberOfLines = 0
        
        verticalStackView.addArrangedSubview(titleLabel)
        verticalStackView.setCustomSpacing(20, after: titleLabel)
        
        horizontalTemperatureStackView.axis = .horizontal
        horizontalTemperatureStackView.spacing = 10
        horizontalTemperatureStackView.translatesAutoresizingMaskIntoConstraints = false
        verticalStackView.addArrangedSubview(horizontalTemperatureStackView)
        
        maxAndMinLabel.setContentHuggingPriority(.required, for: .horizontal)
        maxAndMinLabel.setContentCompressionResistancePriority(.required, for: .horizontal)
        maxAndMinLabel.translatesAutoresizingMaskIntoConstraints = false
        maxAndMinLabel.textAlignment = .left
        horizontalTemperatureStackView.addArrangedSubview(maxAndMinLabel)
        
        currentLabel.translatesAutoresizingMaskIntoConstraints = false
        currentLabel.textAlignment = .right
        horizontalTemperatureStackView.addArrangedSubview(currentLabel)
        
        imageView.contentMode = .scaleAspectFit
        imageView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(imageView)
        
    }
    
    private func setupConstraints() {
        verticalStackView.pinToEdges(of: self, padding: UIEdgeInsets(top: 5, left: 10, bottom: 10, right: 5) )
        
        NSLayoutConstraint.activate([
            imageView.centerYAnchor.constraint(equalTo: self.centerYAnchor),
            imageView.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.35),
            imageView.trailingAnchor.constraint(equalTo: self.trailingAnchor)
        ])
        
        NSLayoutConstraint.activate([
            horizontalTemperatureStackView.leadingAnchor.constraint(equalTo: verticalStackView.leadingAnchor),
            horizontalTemperatureStackView.trailingAnchor.constraint(equalTo: imageView.leadingAnchor, constant: -10)
        ])
    }
    
    func bind(viewModel: WeatherViewModel) {
        
        todayInLabel.text = viewModel.todayInText
        titleLabel.text = viewModel.name
        maxAndMinLabel.text = viewModel.maxAndMinTemp
        currentLabel.text = viewModel.currentTemp
        
        viewModel.imageBox.bind { [weak self] in
            self?.imageView.image = $0
        }
    }
    
}
