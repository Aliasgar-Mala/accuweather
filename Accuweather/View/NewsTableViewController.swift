//
//  NewsTableViewController.swift
//  Accuweather
//
//  Created by Aliasgar Mala on 2020-06-21.
//  Copyright © 2020 Aliasgar Mala. All rights reserved.
//

import UIKit

class NewsTableViewController: UITableViewController {
    private let viewModel: NewsViewModel
    private var rows: [NewsRowViewModel] = [] {
        didSet {
            tableView.reloadData()
        }
    }
    
    init(viewModel: NewsViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        configureBindings()
    }
    
    
    private func configureView() {
        tableView.allowsSelection = false
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 120
        tableView.backgroundColor = .systemGray5
        tableView.register(NewsTableViewCell.self, forCellReuseIdentifier: NewsTableViewCell.defaultReuseIdentifier)
    }
    
    private func configureBindings() {
        
        viewModel.isLoading.bind { [weak self] isLoading in
            isLoading ? self?.view.showActivityIndicator() : self?.view.hideActivityIndicator()
        }
        
        viewModel.rows.bind { [weak self] in
            self?.rows = $0
        }
    }
}

// MARK: - Table view data source

extension NewsTableViewController {

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rows.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let tableViewCell = tableView.dequeueReusableCell(withIdentifier: NewsTableViewCell.defaultReuseIdentifier) as? NewsTableViewCell else {
           preconditionFailure("This should never be nil")
        }
        
        tableViewCell.bind(viewModel: rows[indexPath.row])
        return tableViewCell
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection
                                section: Int) -> String? {
       return "News Highlights"
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60
    }
}
