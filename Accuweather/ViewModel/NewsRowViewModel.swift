//
//  NewsRowViewModel.swift
//  Accuweather
//
//  Created by Aliasgar Mala on 2020-06-21.
//  Copyright © 2020 Aliasgar Mala. All rights reserved.
//

import UIKit

class NewsRowViewModel {
    
    private let article: Article
    private let imageService: ImageService
    let thumbnail: Box<UIImage?> = Box(nil)
    let title: String
    let publisher: String?
    
    init(article: Article, imageService: ImageService) {
        self.article = article
        self.imageService = imageService
        self.title = article.title
        self.publisher = "By \(article.source.name)" 
        downloadThumbnail()
    }
    //loading asynchronously so that other data is shown as soon as it's available
    fileprivate func downloadThumbnail() {
        let placeHolderImage = UIImage(named: "placeholder")
        thumbnail.value = placeHolderImage
        if let imageURLString = article.urlToImage, let url = URL(string: imageURLString) {
            imageService.fetchImage(url: url) { [weak self] image in
                guard let self = self else {return}
                self.thumbnail.value = image
            }
        }
    }
}
