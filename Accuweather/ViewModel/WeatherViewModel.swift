//
//  WeatherViewModel.swift
//  Accuweather
//
//  Created by Aliasgar Mala on 2020-06-17.
//  Copyright © 2020 Aliasgar Mala. All rights reserved.
//

import UIKit

class WeatherViewModel {
    
    var isLoading: Box<Bool> = Box(false)
    let isResponseLoaded: Box<Bool> = Box(false)
    private let weatherService: WeatherService
    private let imageService: ImageService
    var imageBox: Box<UIImage?> = Box(nil)
    var name = ""
    let todayInText = "Today in"
    var maxAndMinTemp = ""
    var currentTemp = ""
    let cityId: Box<Int?> = Box(nil)
    
    init(weatherService: WeatherService, imageService: ImageService) {
        self.weatherService = weatherService
        self.imageService = imageService
        configureBindings()
    }
    
    private func configureBindings() {

        cityId.bind { [weak self] in
            guard let self = self, let cityId = $0 else { return }
            self.isLoading.value = true
            self.weatherService.fetchWeatherInfo(cityId: cityId) { [weak self] result in
                guard let self = self else { return }
                self.isLoading.value = false
                switch result {
                case .success(let currentWeather):
                    self.setResponseParameters(currentWeather)
                case .failure(_):
                    print("handle error")
                }
            }
        }
    }
    
    private func downloadWeatherIcon(icon: String) {
        if !icon.isEmpty {
            let url = URL(string: Endpoint.weatherImage(iconId: icon).path)!
            imageService.fetchImage(url: url) {[weak self] image in
                self?.imageBox.value = image
            }
        }
    }
    
    private func setResponseParameters(_ currentWeather: WeatherInfo) {
        name = currentWeather.name
        let minTemperature = currentWeather.main.tempMin
        let maxTemperature = currentWeather.main.tempMax
        maxAndMinTemp = "max \(Int(minTemperature)) / min \(Int(maxTemperature))"
        
        currentTemp = "Current \(Int(currentWeather.main.temp))"
        downloadWeatherIcon(icon: currentWeather.weather.first?.icon ?? "")
        isResponseLoaded.value = true
    }
}
