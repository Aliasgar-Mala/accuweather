//
//  NewsViewModel.swift
//  Accuweather
//
//  Created by Aliasgar Mala on 2020-06-21.
//  Copyright © 2020 Aliasgar Mala. All rights reserved.
//

import Foundation

class NewsViewModel {
    
    var isLoading: Box<Bool> = Box(false)
    var rows: Box<[NewsRowViewModel]> = Box([])
    private let newsService: NewsService
    private let imageService: ImageService
    
    init(newsService: NewsService, imageService: ImageService) {
        self.newsService = newsService
        self.imageService = imageService
        postInit()
    }
    
    private func postInit() {
        isLoading.value = true
        newsService.getNews() {[weak self] result in
            guard let self = self else { return }
            
            self.isLoading.value = false
            switch result {
                
            case .success(let news):
                self.rows.value = news.articles.map { NewsRowViewModel(article: $0, imageService: self.imageService) }
            case .failure(let error):
                // can show error pop up here
                 print(error)
            }
        }
    }
}
