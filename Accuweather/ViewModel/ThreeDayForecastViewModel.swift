//
//  ThreeDayForecastViewModel.swift
//  Accuweather
//
//  Created by Aliasgar Mala on 2020-06-20.
//  Copyright © 2020 Aliasgar Mala. All rights reserved.
//

import Foundation
import UIKit

enum DayNumber {
    case one
    case two
    case three
    
    var dateString: String {
        switch self {
        case .one:
            return Date().getDatePlus(numberOfDays: 1) //tomorrow
        case .two:
            return Date().getDatePlus(numberOfDays: 2) //day after tomorrow
        case .three:
            return Date().getDatePlus(numberOfDays: 3) // 2 days after tomorrow
        }
    }
}
class ThreeDayForecastViewModel {
    
    private let weatherService: WeatherService
    private let imageService: ImageService
    var isLoading: Box<Bool> = Box(false)
    private var weatherInfo: [ForeCastInfo] = []
    var minMax1 = ""
    var image1: Box<UIImage?> = Box(nil)
    var minMax2 = ""
    var image2: Box<UIImage?> = Box(nil)
    var minMax3 = ""
    var image3: Box<UIImage?> = Box(nil)
    let isResponseLoaded: Box<Bool> = Box(false)
    let cityId: Box<Int?> = Box(nil)

    init(weatherService: WeatherService, imageService: ImageService) {
        self.weatherService = weatherService
        self.imageService = imageService
        configureBindings()
    }

    fileprivate func setResponseParameter(_ forecast: (ForecastWeather)) {
        self.weatherInfo = forecast.threeHourlyWeatherInfo
        self.getImageIcon(imageBox: self.image1, day: .one)
        self.minMax1 = self.getMinMax(day: .one)
        self.getImageIcon(imageBox: self.image2, day: .two)
        self.minMax2 = self.getMinMax(day: .two)
        self.getImageIcon(imageBox: self.image3, day: .three)
        self.minMax3 = self.getMinMax(day: .three)
        self.isResponseLoaded.value = true
    }
    
    private func configureBindings() {
        cityId.bind { [weak self] in
            guard let self = self, let cityId = $0 else { return }
            self.isLoading.value = true
            self.weatherService.get3dayForecast(cityId: cityId) { [weak self] result in
                guard let self = self else { return }
                self.isLoading.value = false
                switch result {
                case .success(let forecast):
                    self.setResponseParameter(forecast)
                case .failure(let error):
                    // can show error pop up here
                    print(error)
                }
            }
        }
    }
    
    //calculate the max and min of daily available weather
    private func getMinMax(day: DayNumber) -> String {
        var result = ""
        //filter dates based on date text
        let filterDates = weatherInfo.filter { $0.dateText.hasPrefix(day.dateString)}
        if let maxTemperature = filterDates.map({ $0.main.tempMax}).max(),
            let minTemperature = filterDates.map({ $0.main.tempMin}).min() {
            result = "\(Int(maxTemperature))/\(Int(minTemperature))"
        }
        
        return result
    }
    
    //get the icon which appears maximum time in weather for the day
    private func getImageIcon(imageBox: Box<UIImage?>, day: DayNumber) {
        
        var frequencyDict: [String:Int] = [:]
        let filterDates = weatherInfo.filter { $0.dateText.hasPrefix(day.dateString)}
        let weatherDescription = filterDates.map{ $0.weather.first!.main }
        for weather in weatherDescription {
            frequencyDict[weather, default:0] += 1
        }
        let max = frequencyDict.values.max()
        let maxWeatherOccurrence = frequencyDict.first { $0.value == max }?.key
        
        if let icon = filterDates.first(where: { $0.weather.first!.main == maxWeatherOccurrence })?.weather.first?.icon {
            let url = URL(string: Endpoint.weatherImage(iconId: icon).path)!
            imageService.fetchImage(url: url) { image in
                imageBox.value = image
            }
        }
    }
}
