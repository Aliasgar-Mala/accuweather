//
//  Setup.swift
//  Accuweather
//
//  Created by Aliasgar Mala on 2020-06-22.
//  Copyright © 2020 Aliasgar Mala. All rights reserved.
//

import Foundation

//Entry point of the application where all initialization should be done
public class AccuweatherSetup {
    public static let shared = AccuweatherSetup()
    var viewControllerFactory: ViewControllerFactory!
    var cities: [City] = []
    
    private init() {}
    
    public static func initialize() {
        let service = ServiceLayerClient()
        shared.viewControllerFactory = ViewControllerFactory(service: service)
        shared.loadFile()
    }
    
    private func loadFile() {
        //this can be done back ground thread if required. Since it's just a single screen
        //I have done it on the main thread so that app will only load once this list is loaded
       if let path = Bundle.main.path(forResource: "city.list.min", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let decoder = JSONDecoder()
                self.cities = try decoder.decode([City].self, from: data)

            } catch {
                 preconditionFailure("loading of file failed crash here")
            }
        }
    }
}
